#include <iostream>
#include <iterator>

#include "drawing.h"
#include "rectintersection.h"

Drawing::Drawing()
{

}

Drawing::~Drawing()
{
    // explicit delete
    while(!elements.empty()) delete elements.front(), elements.pop_front();
}

void Drawing::Init()
{
    SortElements();

    // print elment info
    PrintAllElements();
}

void Drawing::AddElement(Element *element)
{
    elements.push_back(element);
}

void Drawing::PrintAllElements()
{
    std::cout << "Input (only the first 10):" << std::endl;
    for (std::list<Element*>::iterator it=elements.begin(); it != elements.end(); ++it) {
        (*it)->Print();
    }
}

void Drawing::FindIntersectionOfaIntersection(Element* elem_intersection,
                                              std::list<Element*> &intersections,
                                              std::list<Element*>::iterator it/*don't need start from the begin*/)
{
    for ( ;it != elements.end(); ++it) {
        Element *new_elem = elem_intersection->GetIntersection(*it);
        if(new_elem){
            RectIntersection *rectIntersection = dynamic_cast<RectIntersection*>(elem_intersection);
            RectIntersection *new_rectIntersection = dynamic_cast<RectIntersection*>(new_elem);

            // add the new intersection in the global intersection
            intersections.push_back(new_rectIntersection);

            // add the rect references where this intersection come from
            new_rectIntersection->AddIntersectionElement(dynamic_cast<Rect*>(*it));

            // add the rect list references where this intersection come from
            new_rectIntersection->AddIntersectionElement(rectIntersection->rectIntersections);

            // go find more...
            FindIntersectionOfaIntersection(new_rectIntersection,intersections, std::next(it, 1));
        }
    }
}

void Drawing::FindIntersection(std::list<Element*> &interElements)
{

    for (std::list<Element*>::iterator it=elements.begin(); it != elements.end(); ++it) {
        for (std::list<Element*>::iterator it2=std::next(it, 1); it2 != elements.end(); ++it2) {
            Element *ele = (*it)->GetIntersection(*it2);
            if(ele){
                RectIntersection *rectInter = dynamic_cast<RectIntersection*>(ele);

                // add the new intersection in the global intersection
                interElements.push_back(rectInter);

                // add the rect references where this intersection come from
                rectInter->AddIntersectionElement(dynamic_cast<Rect*>(*it));
                rectInter->AddIntersectionElement(dynamic_cast<Rect*>(*it2));

                // verify if this intersection can intersect other elements
                FindIntersectionOfaIntersection(ele,interElements, std::next(it2, 1));
            }
        }
    }
}

// sort the element list by coordinates, you MUST have to call this function before try to find intersections
void Drawing::SortElements()
{
    // trying to use some nice things from c++11
    elements.sort([](Element * lhs, Element * rhs) {return lhs->GetPosX() < rhs->GetPosX();});

    // re index of all elements
    int cont = 1;
    for (std::list<Element*>::iterator it=elements.begin(); it != elements.end(); ++it) {
        (*it)->SetId(cont++);
    }

}

// sort and print the intersection elements
// sort by rectangle id and numbers of rectangles are include in the intersection
void Drawing::PrintIntesectionElements(std::list<Element*> &interElements)
{
    // first sort the list of rectangles are include in the intersection
    for (std::list<Element*>::iterator it=interElements.begin(); it != interElements.end(); ++it) {
        dynamic_cast<RectIntersection*>(*it)->rectIntersections.sort([](Element * lhs, Element * rhs)
        {
            return lhs->GetId() < rhs->GetId();
        });
    }

    // second sort the list of intersection
    interElements.sort([](Element * lhs, Element * rhs)
    {
        RectIntersection* r1 = dynamic_cast<RectIntersection*>(lhs);
        RectIntersection* r2 = dynamic_cast<RectIntersection*>(rhs);
        return r1->rectIntersections.size() < r2->rectIntersections.size();
    });

    // output
    std::cout << "Intersections " << std::endl;
    for (std::list<Element*>::iterator it=interElements.begin(); it != interElements.end(); ++it) {
        (*it)->Print();
    }
}
