#ifndef DRAWING_H
#define DRAWING_H

#include <list>
#include "element.h"

class Drawing
{
public:
    Drawing();
    ~Drawing();

    void AddElement(Element* element);

    std::list<Element*>* GetElements();

    void PrintAllElements();

    void FindIntersection(std::list<Element *> &interElements);

    void SortElements();

    void Init();

    void PrintIntesectionElements(std::list<Element *> &interElements);
private:

    // global list of Elements
    // caution, this is a list of *Element so you must management the objects.
    std::list<Element*> elements;

    void FindIntersectionOfaIntersection(Element *elem_intersection,
                                         std::list<Element *> &intersections,
                                         std::list<Element *>::iterator start_it);
};

#endif // DRAWING_H
