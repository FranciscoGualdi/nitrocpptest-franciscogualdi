#include <iostream>

#include "element.h"


Element::Element(int _posX, int _posY)
{
    posX = _posX;
    posY = _posY;
    id = -1;
}

Element::~Element()
{
}

int Element::GetPosX()
{
   return posX;
}

int Element::GetPosY()
{
    return posY;
}

void Element::Print()
{
}
