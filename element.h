#ifndef ELEMENT_H
#define ELEMENT_H


class Element
{
public:
    Element(int posX, int posY);
    virtual ~Element();

    int GetId(){return id;}
    void SetId(int _id){id = _id;}

    int GetPosX();
    int GetPosY();

    virtual void Print();

    virtual Element* GetIntersection(Element *other) = 0;

private:

    int id;
    int posX;
    int posY;
};

#endif // ELEMENT_H
