#include <iostream>
#include <string>
#include "readjsonrect.h"
#include "rapidjson/document.h"
#include "rapidjson/filereadstream.h"

using namespace rapidjson;

ReadJsonRect::ReadJsonRect(int max)
{
    maxRect = max;
}

void ReadJsonRect::GetRects(std::string fileName, std::list<Rectangles> &rects)
{
    //TODO filename and location maybe can be config or by input
    FILE* fp = fopen(fileName.c_str(), "rb");
    if(fp == NULL) {
        //TODO more accurate messages erros
        std::cout << "File "<< fileName.c_str() << "not found. Put it with the bynary." << std::endl;
        return;
    }

    char readbuffer[65536];
    FileReadStream is(fp, readbuffer, sizeof(readbuffer));

    Document d;
    d.ParseStream(is);

    fclose(fp);

    assert(d.IsObject());

    const Value& rectsJson = d["rects"];
    assert(rectsJson.IsArray());

    for (SizeType var = 0; var < rectsJson.Size(); var++) {
        struct Rectangles rect;

        assert(rectsJson[var].HasMember("x"));
        assert(rectsJson[var]["x"].IsInt());
        rect.posx = rectsJson[var]["x"].GetInt();

        assert(rectsJson[var].HasMember("y"));
        assert(rectsJson[var]["y"].IsInt());
        rect.posy = rectsJson[var]["y"].GetInt();

        assert(rectsJson[var].HasMember("w"));
        assert(rectsJson[var]["w"].IsInt());
        rect.w = rectsJson[var]["w"].GetInt();

        assert(rectsJson[var].HasMember("h"));
        assert(rectsJson[var]["h"].IsInt());
        rect.h = rectsJson[var]["h"].GetInt();

        rects.push_back(rect);
        // put the limit inside the loop, maybe we can discard invalide elements
        if(maxRect == rects.size())
            break;
    }
}

// just for test.
void ReadJsonRect::GetRectsMock(int id, std::list<Rectangles> &rects)
{
    switch (id) {
    case 1:
        GetRectsMock1(rects);
        break;
    case 2:
        GetRectsMock2(rects);
        break;
    case 3:
        GetRectsMock3(rects);
        break;
    default:
        break;
    }\

}


// just for test.
void ReadJsonRect::GetRectsMock1(std::list<Rectangles> &rects)
{
    std::cout << " <<<<<<<<<<<<<< Testing Mock 1 >>>>>>>>>>>>" << std::endl;

    struct Rectangles rect1;
    struct Rectangles rect2;
    struct Rectangles rect3;
    struct Rectangles rect4;

    rect1.posx = 100;
    rect1.posy = 100;
    rect1.w = 250;
    rect1.h = 80;

    rect2.posx = 120;
    rect2.posy = 200;
    rect2.w = 250;
    rect2.h = 150;

    rect3.posx = 140;
    rect3.posy = 160;
    rect3.w = 250;
    rect3.h = 100;

    rect4.posx = 160;
    rect4.posy = 140;
    rect4.w = 350;
    rect4.h = 190;

    rects.push_back(rect2);
    rects.push_back(rect4);
    rects.push_back(rect1);
    rects.push_back(rect3);
}

// just for test.
void ReadJsonRect::GetRectsMock2(std::list<Rectangles> &rects)
{
    std::cout << " <<<<<<<<<<<<<< Testing Mock 2 >>>>>>>>>>>>" << std::endl;
    struct Rectangles rect1;
    struct Rectangles rect2;
    struct Rectangles rect3;
    struct Rectangles rect4;
    struct Rectangles rect5;


    rect1.posx = 80;
    rect1.posy = 50;
    rect1.w = 400;
    rect1.h = 400;

    rect2.posx = 100;
    rect2.posy = 100;
    rect2.w = 250;
    rect2.h = 80;

    rect3.posx = 120;
    rect3.posy = 200;
    rect3.w = 250;
    rect3.h = 150;

    rect4.posx = 140;
    rect4.posy = 160;
    rect4.w = 250;
    rect4.h = 100;

    rect5.posx = 160;
    rect5.posy = 140;
    rect5.w = 350;
    rect5.h = 190;

    rects.push_back(rect1);
    rects.push_back(rect2);
    rects.push_back(rect3);
    rects.push_back(rect4);
    rects.push_back(rect5);
}

// just for test.
void ReadJsonRect::GetRectsMock3(std::list<Rectangles> &rects)
{
    std::cout << " <<<<<<<<<<<<<< Testing Mock 3 >>>>>>>>>>>>" << std::endl;
    struct Rectangles rect1;
    struct Rectangles rect2;
    struct Rectangles rect3;
    struct Rectangles rect4;
    struct Rectangles rect5;
    struct Rectangles rect6;
    struct Rectangles rect7;
    struct Rectangles rect8;
    struct Rectangles rect9;
    struct Rectangles rect10;

    rect1.posx = 100;
    rect1.posy = 100;
    rect1.w = 250;
    rect1.h = 80;

    rect2.posx = 120;
    rect2.posy = 200;
    rect2.w = 250;
    rect2.h = 150;

    rect3.posx = 140;
    rect3.posy = 160;
    rect3.w = 250;
    rect3.h = 100;

    rect4.posx = 160;
    rect4.posy = 140;
    rect4.w = 350;
    rect4.h = 190;

    rect5.posx = 100;
    rect5.posy = 100;
    rect5.w = 250;
    rect5.h = 80;

    rect6.posx = 120;
    rect6.posy = 200;
    rect6.w = 250;
    rect6.h = 150;

    rect7.posx = 140;
    rect7.posy = 160;
    rect7.w = 250;
    rect7.h = 100;

    rect8.posx = 160;
    rect8.posy = 140;
    rect8.w = 350;
    rect8.h = 190;

    rect9.posx = 100;
    rect9.posy = 100;
    rect9.w = 250;
    rect9.h = 80;

    rect10.posx = 120;
    rect10.posy = 200;
    rect10.w = 250;
    rect10.h = 150;

    rects.push_back(rect1);
    rects.push_back(rect2);
    rects.push_back(rect3);
    rects.push_back(rect4);
    rects.push_back(rect5);
    rects.push_back(rect6);
    rects.push_back(rect7);
    rects.push_back(rect8);
    rects.push_back(rect9);
    rects.push_back(rect10);
}

bool ReadJsonRect::ValidateRectsMock(int id, std::list<Rectangles> &rects)
{
    bool value = false;
    switch (id) {
    case 1:
        value = ValidateRectsMock1(rects);
        break;
    case 2:
        value = ValidateRectsMock2(rects);
        break;
    case 3:
        value = ValidateRectsMock3(rects);
        break;
    default:
        break;
    }
    return value;
}

bool ReadJsonRect::ValidateRectsMock1(std::list<Rectangles> &rects)
{
    int result_size = 7;

    return false;
}

bool ReadJsonRect::ValidateRectsMock2(std::list<Rectangles> &rects)
{
    return false;

}

bool ReadJsonRect::ValidateRectsMock3(std::list<Rectangles> &rects)
{
    return false;

}
