#ifndef READJSONRECT_H
#define READJSONRECT_H

#include <list>

struct Rectangles {
   double  posx;
   double  posy;
   double  w;
   double  h;
};

class ReadJsonRect
{
public:
    ReadJsonRect(int maxRect);

    void GetRects(std::string file, std::list<Rectangles> &rects);

    // just for test, getting some data
    void GetRectsMock(int id, std::list<Rectangles> &rects);
    void GetRectsMock1(std::list<Rectangles> &rects);
    void GetRectsMock2(std::list<Rectangles> &rects);
    void GetRectsMock3(std::list<Rectangles> &rects);

    // just for test, getting some result
    bool ValidateRectsMock(int id, std::list<Rectangles> &rects);
    bool ValidateRectsMock1(std::list<Rectangles> &rects);
    bool ValidateRectsMock2(std::list<Rectangles> &rects);
    bool ValidateRectsMock3(std::list<Rectangles> &rects);

private:
    int maxRect; // maxime number of rect must read

};

#endif // READJSONRECT_H
