#include <iostream>

#include "rect.h"
#include "rectintersection.h"


Rect::Rect(double posX, double posY, double w, double h)
    : Element(posX, posY)
{
    p2.x = posX + w;
    p2.y = posY + h;
}

Rect::~Rect()
{
}

double Rect::GetWidth()
{
    return (p2.x - GetPosX());
}

double Rect::GetHeight()
{
    return (p2.y - GetPosY());
}

Point2D Rect::GetPoint2D_1()
{
    Point2D p1;
    p1.x = GetPosX();
    p1.y = GetPosY();
    return (p1);
}

Point2D Rect::GetPoint2D_2()
{
    return p2;
}

void Rect::Print()
{
    std::cout << GetId() << ": Rectangle " << "at (" << GetPosX() << "," << GetPosY() << "), w=" << GetWidth() << ", h=" << GetHeight() << std::endl;
}

Element *Rect::GetIntersection(Element *other)
{
    Rect *otherRect = dynamic_cast<Rect*>(other);

    double x1 = std::max(GetPoint2D_1().x, otherRect->GetPoint2D_1().x);
    double y1 = std::max(GetPoint2D_1().y, otherRect->GetPoint2D_1().y);
    double x2 = std::min(GetPoint2D_2().x, otherRect->GetPoint2D_2().x);
    double y2 = std::min(GetPoint2D_2().y, otherRect->GetPoint2D_2().y);

    if((x1 >= x2) || (y1 >= y2))
    {
        return nullptr;
    }
    else{
        return new RectIntersection(x1,y1,x2-x1,y2-y1);
    }
}
