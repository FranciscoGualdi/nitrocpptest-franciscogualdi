#ifndef RECT_H
#define RECT_H

#include "element.h"

struct Point2D{
    double x;
    double y;
};

class Rect: public Element
{
public:
    Rect(double posX, double posY, double w, double h);
    virtual ~Rect();

    double GetWidth();
    double GetHeight();

    Point2D GetPoint2D_2();
    Point2D GetPoint2D_1();

    virtual Element *GetIntersection(Element *other);

    virtual void Print();
private:
    Point2D p2;
};

#endif // RECT_H
