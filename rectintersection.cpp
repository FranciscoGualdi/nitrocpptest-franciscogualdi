#include <iostream>
#include "rectintersection.h"

RectIntersection::RectIntersection(double posX, double posY, double w, double h)
    :Rect(posX, posY, w, h)
{

}

RectIntersection::~RectIntersection()
{

}

void RectIntersection::AddIntersectionElement(std::list<Rect*> intersections)
{
    for (std::list<Rect*>::iterator it=intersections.begin();
         it != intersections.end(); ++it) {
         AddIntersectionElement(*it);
    }
}

void RectIntersection::AddIntersectionElement(Rect* rect)
{
    // TODO need improvement
    bool found = false;
    for (std::list<Rect*>::iterator it=rectIntersections.begin(); it != rectIntersections.end(); ++it)
    {
        if(rect == (*it)){
            found = true;
            break;
        }
    }

    if(!found)
        rectIntersections.push_back(rect);
}

void RectIntersection::Print()
{
    std::cout << "Between rectangle ";

    int cont = 0;
    for (std::list<Rect*>::iterator it=rectIntersections.begin(); it != rectIntersections.end(); ++it) {
        if(rectIntersections.size() == ++cont)
            std::cout << " and ";
        std::cout << (*it)->GetId();
        if(rectIntersections.size() > cont+1)
            std::cout << ", ";
    }

    std::cout << " at (" << GetPosX() << "," << GetPosY() << "), w=" << GetWidth() << ", h=" << GetHeight() << std::endl;
}

