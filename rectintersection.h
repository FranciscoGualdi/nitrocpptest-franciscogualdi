#ifndef RECTINTERSECTION_H
#define RECTINTERSECTION_H

#include <list>

#include "rect.h"

class RectIntersection : public Rect
{
public:
    RectIntersection(double posX, double posY, double w, double h);
    ~RectIntersection();  
    std::list<Rect*> rectIntersections;
    void Print();
    void AddIntersectionElement(Rect *rect);
    void AddIntersectionElement(std::list<Rect*> intersections);
};

#endif // RECTINTERSECTION_H
