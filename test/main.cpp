#include <iostream>
#include <vector>
#include <fstream>
#include <string>

#include "../readjsonrect.h"
#include "../rect.h"
#include "../drawing.h"

using namespace std;

// main of the tests
int main(int argc, char* argv[])
{
    // our test is based on compare the output's
    if (argc < 2) {
        // Tell the user how to run the program
        std::cout << "Usage: " << argv[0] << " sample.json" << std::endl;
        return 1;
    }

    // PATH name coming from cmake need to been clean, only need the file name.
    // TODO caution with these hardcoded sizes test_9.json!!! need improve this situation
    string in = argv[1];
    //redirect std::cout to output.txt
    string cleanned = (in.size()>11)?in.substr(in.size()-11,in.size()):in;
    string _output = cleanned.substr(0,cleanned.size()-5);
    _output.insert(0, "out_");
    _output.append(".txt");
    std::cout << "FILE: " << _output.c_str() << std::endl;

    std::ofstream out(_output.c_str());
    std::cout.rdbuf(out.rdbuf());

    // create a Drawing, a place for all elements and some operations
    Drawing *drawing = new Drawing();

    // read the json file
    std::list<Rectangles> rectsFromJson;
    ReadJsonRect rjr(10 /*max number of rectangles*/);
    rjr.GetRects(argv[1], rectsFromJson);

    // parsing the rects from json to our Rect element
    for (std::list<Rectangles>::iterator it=rectsFromJson.begin(); it != rectsFromJson.end(); ++it)
    {
        struct Rectangles r_struct = *it;
        Rect *rect = new Rect(r_struct.posx, r_struct.posy, r_struct.w, r_struct.h);
        drawing->AddElement(rect);
    }

    drawing->Init();

    // global list of intersections
    std::list<Element*> interElements;

    //Trying to find the intersection
    drawing->FindIntersection(interElements);

    //output
    drawing->PrintIntesectionElements(interElements);

    // explicit delete interElements
    while(!interElements.empty()) delete interElements.front(), interElements.pop_front();

    delete drawing;

    return 0;
}
